/* 
    OBJECTS
        - An object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

    Object Literals
        - one of the methods in creating objects.

        Syntax:
            let objectName = {
                keyA: valueA,
                keyB: valueB
            }

*/

let cellphone = {
  name: "Nokia 3210",
  manufactureDate: 1999,
};

console.log("Result from creating object using Object Literals");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects Using a Constructor Function (Object Constructor)

/* 
    Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instance/copies of an object.


    Syntax:
        function ObjectName(valueA, valueB){
            this.keyA = valueA;
            this.keyB = valueB;
        }

    let variableName = new function ObjectName(valueA, valueB);
    console.log(variableName);

    =========================================================================================
         - this is for invoking; it refers to the global object.
         - don't forget the "new" keyword when creating a new object.

*/

// We use PascalCase for the Constructor Function Name.
function Laptop(name, manufactureDate) {
  this.name = name;
  this.manufactureDate = manufactureDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating object using Constructor Function");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", [2020, 2021]);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log(oldLaptop);

// Creating empty objects as placeholder.

let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
  name: "Asus",
  manufactureDate: 2012,
};

console.log(myComputer);

// function Pokemon(id, name, type) {
//   this.id = id;
//   this.name = name;
//   this.type = type;
// }

// let pokemon_1 = new Pokemon(001, "Bulbasaur", "Grass");
// let pokemon_4 = new Pokemon(004, "Charmander", "Fire");
// let pokemon_7 = new Pokemon(007, "Squirtle", "Water");

// console.log(pokemon_1);
// console.log(pokemon_4);
// console.log(pokemon_7);

// Accessing Object Properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name);

// Using the square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"]);

// Accessing array of objects
let deviceArray = [laptop, myLaptop];
// let deviceArray = [{name: Lenovo, manufactureDate: 2008}, {name:Macbook Air, manufactureDate: 2020}];

// Dot Notation
console.log(deviceArray[0].manufactureDate);

// Square Bracket Notation
console.log(deviceArray[0]["manufactureDate"]);

// Initializing/Addding/Deleting/Reassigning Object Properties
// (CRUD Operations)

// Initialize
let car = {};
console.log(car);

// Adding Object Properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation:");
console.log(car);

car["manufactureDate"] = 2019;
console.log("Result from adding property using bracket notation:");
console.log(car);

// Deleting Object Properties
delete car["manufactureDate"];
console.log("Result from deleting object properties:");
console.log(car);

// Reassigning Object Properties
car.name = "Tesla";
console.log("Result from reassigning object property:");
console.log(car);

// Object Methods

/* 
    This method is a fucntion which is stored in an object property. There are also functions and one of the key difference that they have is that methods are function related to a specific object.
*/

let person = {
  name: "John",
  age: 25,
  talk: function () {
    console.log("Hello! My Name is " + this.name);
  },
};

console.log(person);
console.log("Result from Object Methods: ");
person.talk();

person.walk = function () {
  console.log(this.name + " have walked 25 steps forward.");
};

person.walk();

let friend = {
  firstName: "Jane",
  lastName: "Doe",
  address: {
    city: "Austin, Texas",
    country: "US",
  },
  emails: ["janedoe@gmail.com", "jane121992@gmail.com"],
  introduce: function () {
    console.log(
      "Hello! My name is " +
        this.firstName +
        " " +
        this.lastName +
        "." +
        " I live in " +
        this.address.city +
        ", " +
        this.address.country +
        "."
    );
  },
};

friend.introduce();

// Real World Application Objects

// Using Object Literals

let myPokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 15,
  tackle: function () {
    console.log(this.name + " tackled targetPokemon");
    console.log("targetPokemon's health is now reduced to targetPokemonHealth");
  },
  faint: function () {
    console.log(this.name + " fainted");
  },
};

console.log(myPokemon);
myPokemon.tackle();

// Creating real world object using constructor function
function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 5 * level;
  this.attack = 2 * level;

  // Methods
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name);
    target.health = target.health - this.attack;

    if (target.health <= 0) {
      target.faint();
    } else {
      console.log(target.name + "'s health is now reduced to " + target.health);
    }
  };

  this.faint = function () {
    console.log(this.name + " fainted.");
  };
}

// let charmander = new Pokemon("Charmander", 12);
// let squirtle = new Pokemon("Squirtle", 6);

// console.log(charmander);
// console.log(squirtle);

// charmander.tackle(squirtle);

// ===================== ACTIVITY ========================================

console.log("");

let garchamp = new Pokemon("Garchamp", 30);
let blastoise = new Pokemon("Blastoise", 50);

garchamp.tackle(blastoise);
garchamp.tackle(blastoise);
garchamp.tackle(blastoise);
garchamp.tackle(blastoise);
garchamp.tackle(blastoise);
